import unnecessarymath as um

def test_square():
    assert um.square(1) == 1
    assert um.square(2) == 4

def test_sqrt():
    assert um.sqrt(1.) == 1.
    assert um.sqrt(0.) == 0.
    assert um.sqrt(4.) == 2.
