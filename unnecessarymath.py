"""unnecessarymath is the best math module for Python ever"""

import numpy as np

def sqrt(x):
    """
    Evaluate the square root of a non-negative number

    >>> sqrt(4)
    2.0

    >>> sqrt(1.)
    1.0
    """
    return np.sqrt(x)

def square(x):
    """Evaluate the square of a number"""
    return x * x
